//
//  AboutUsViewController.swift
//  CarExhaustCalc
//
//  Created by Milo Gilad on 12/7/18.
//  Copyright © 2018 NBPS. All rights reserved.
//

import UIKit

class AboutUsViewController: UIViewController, UITabBarDelegate {
    @IBOutlet weak var aboutUsTabBar: UITabBar!
    @IBOutlet weak var aboutUsTabBarItem: UITabBarItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        aboutUsTabBar.delegate = self;
        
        // Do any additional setup after loading the view.
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if item.tag == 0 {
            performSegue(withIdentifier: "showHome", sender: self);
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
