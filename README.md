# Project: Car Exhaust Calc App

Advanced Swift Programming
North Broward Preparatory School

Started: December 6, 2018
End: December 13, 2018

Mr. Edward Bujak

Jay Gopal

Sam Lewittes

Cannon Johns

Zachary Maizes

Megan Mui

Milo Gilad

The Google sub-folder for artifacts/assets for this project are:
[https://drive.google.com/open?id=1n3IWLKh6OFeiBh4PalrfQqQKEqOcV03Q](https://drive.google.com/open?id=1n3IWLKh6OFeiBh4PalrfQqQKEqOcV03Q)
